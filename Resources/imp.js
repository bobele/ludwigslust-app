var win = Titanium.UI.currentWindow;

var data = [
	{title:'Anfänge',path:'anfaenge', hasChild:true, url:'imp_gallerie.js', count:'4',color:'#fff'},
	{title:'Historische Ansichten',path:'historische_ansichten', hasChild:true, url:'impression_gallerie.js', count:'41', color:'#fff'},
	{title:'Historische Personen',path:'historische_personen', hasChild:true, url:'impression_gallerie.js', count:'7', color:'#fff'},
	{title:'Jahreszeiten',path:'jahreszeiten', hasChild:true, url:'impression_gallerie.js', count:'42', color:'#fff'},
	{title:'Schloss',path:'schloss', hasChild:true, url:'impression_gallerie.js', count:'21', color:'#fff'},
	{title:'Schlosspark',path:'schlosspark', hasChild:true, url:'impression_gallerie.js', count:'40', color:'#fff'},
	{title:'Schlossplatz',path:'schlossplatz', hasChild:true, url:'impression_gallerie.js', count:'13', color:'#fff'},
	{title:'Stadtansichten',path:'stadtansichten', hasChild:true, url:'impression_gallerie.js', count:'36', color:'#fff'},
	{title:'Stadtkirche',path:'stadtkirche', hasChild:true, url:'impression_gallerie.js', count:'8', color:'#fff'},
];


// create table view
var tableview = Titanium.UI.createTableView({
	data:data,
	backgroundColor:'#8eca66',
	
});

// create table view event listener
tableview.addEventListener('click', function(e)
{
	if (e.rowData.url)
	{
		var win = null;
		if (Ti.Platform.name == "android") {
			win = Titanium.UI.createWindow({
				url:'imp_gallerie.js',
				title:e.rowData.title,
				backgroundColor:'#fff',
			});
		} else {
			win = Titanium.UI.createWindow({
				url:'imp_gallerie.js',
				title:e.rowData.title,
				backgroundColor:'#fff',
				barColor:'#111'
			});
		}
		win.count = e.rowData.count;
		win.path = e.rowData.path;
		win.open({modal:true, animated:true});
	}
});

win.add(tableview);
