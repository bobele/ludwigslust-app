var win = Titanium.UI.currentWindow;
var data = new Array();

data[0] = Ti.UI.createTableViewRow({
		width:'auto',
		height:'auto',
		color:'#fff',title:'Bei Hofe',hasChild:true, url:'map.js', auswahl:'1',
		backgroundColor:'#8eca66'
		});
		
data[1] = Ti.UI.createTableViewRow({
		width:'auto',
		height:'auto',
		color:'#fff',title:'Stadtkultur',hasChild:true, url:'map.js', auswahl:'2',
		backgroundColor:'#8eca66'
		});
data[2] = Ti.UI.createTableViewRow({
		width:'auto',
		height:'auto',
		color:'#fff',title:'Schlosspark',hasChild:true, url:'map.js', auswahl:'3',
		backgroundColor:'#8eca66'
		});
data[3] = Ti.UI.createTableViewRow({
	width:'auto',
	height:'auto',
	color:'#fff',title:'alle Stationen',hasChild:true, url:'map.js', auswahl:'0',
	backgroundColor:'#8eca66'
	});

/*		
	{title:'Bei Hofe', hasChild:true, url:'map.js', auswahl:'1', color:'#fff'},
	{title:'Stadtkultur', hasChild:true, url:'map.js', auswahl:'2', color:'#fff'},
	{title:'Schlosspark', hasChild:true, url:'map.js', auswahl:'3', color:'#fff'},
	{title:'alle Stationen', hasChild:true, url:'map.js', auswahl:'0',color:'#fff'},
];
*/
// create table view
var tableview = Titanium.UI.createTableView({
	data:data,
	backgroundColor:'#8eca66',
});

// create table view event listener
tableview.addEventListener('click', function(e)
{
	if (e.rowData.url)
	{
		var win = null;
		if (Ti.Platform.name == "android") {
			win = Titanium.UI.createWindow({
				url:e.rowData.url,
				title:e.rowData.title
			});
		} else {
			win = Titanium.UI.createWindow({
				url:e.rowData.url,
				title:e.rowData.title,
				backgroundColor:'#fff',
				barColor:'#111'
			});
		}
		win.auswahl = e.rowData.auswahl;
		win.open({modal:true, animated:true});
	}
});
win.add(tableview);
