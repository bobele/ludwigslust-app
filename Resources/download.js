var win = Titanium.UI.currentWindow;
win.backgroundColor = '#8eca66';


var dir = Titanium.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory);
var space = parseInt(dir.spaceAvailable());
var space_in_kb = space / 1024;
var space_in_mb = parseInt(space_in_kb / 1024);

var fileDownload = Titanium.UI.createButton({
	title:'Dateien runterladen',
	height:'auto',
	width:'auto',
	top:220
});

if(space_in_mb > 165)
{
win.add(fileDownload);	
}

var label = Titanium.UI.createLabel({
	font:{fontSize:18},
	color:'#fefefe',
	top:40,
	left:10,
	width:'auto',
	height:'auto',
	text:'Bitte laden Sie die benötigten Audiodateien herunter.' 
});

win.add(label);


Titanium.Network.addEventListener('change', function(e)
{
	var type = e.networkType;
	var online = e.online;
	var networkTypeName = e.networkTypeName;
	if(online==true && type==1)
	{
		label.text = 'Bitte laden Sie die benötigten Audiodateien herunter.'
	}
	if(online==true && type==2)
	{
		label.text = 'Bitte laden Sie die benötigten Audiodateien herunter. Wir empfehlen hierfür eine W-LAN Verbindung.'
	}
	if(online==false)
	{
		label.text = 'Es wird eine Internetverbindung benötigt, um die Audiodateien herunterzuladen. Wir empfehlen hierfür eine W-LAN Verbindung. '
	}
	if(space_in_mb <= 165)
	{
		label.text = 'Es ist nicht genug Speicher zur Verfügung. Bitte schaffen genug Platz für die Audiodateien. Es werden 165 MB Speicher beansprucht.';
	}
});


var ind=Titanium.UI.createProgressBar({
	width:200,
	height:50,
	min:0,
	max:1,
	value:0,
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	top:0,
	message:'Ladestatus der aktuellen Datei',
	font:{fontSize:12, fontWeight:'bold'},
	color:'#888'
});

var actInd = Titanium.UI.createActivityIndicator({
		location:Titanium.UI.ActivityIndicator.DIALOG,
		type:Titanium.UI.ActivityIndicator.DETERMINANT,
		message:'Lade 1 von 57',
		min:0,
		max:57,
		value:0,
		cancelable:true
});


fileDownload.addEventListener('click', function()
{

	// show Indicator
	win.add(ind);
	actInd.show();
	ind.show();	
	//Stationen aus der DB ins Array


	function download(number){
		var c = Titanium.Network.createHTTPClient();
		c.setTimeout(10000);
		var audioFile = c;
		actInd.setValue(counter);
		actInd.setMessage('Geladen ' + counter + ' von ' + number) ; 
		ind.value = 0;
		var f = Titanium.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory,counter + '.mp3');

		if(f.exists() == true)
		{
			counter += 1;
			Ti.API.info(counter +' | ' + c + 'exists = ' + f.exists());
			Ti.API.info('size = ' + f.size);
			Ti.API.info('nativePath = ' + f.nativePath);
			
			if(counter<number)
			{
				//c = null;
				actInd.setValue(counter);
				actInd.setMessage('Lade ' + counter + ' von ' + number) ; 
				download(57);
			}else
			{
				c = null;
				Ti.App.Properties.setString("audioComplete","yes")
				ind.hide();	
				actInd.hide();
				return;
			}
			
		}else
		{
			Ti.API.info(counter +' | ' +audioFile + 'exists = ' + f.exists());
			Ti.API.info('nativePath = ' + f.nativePath);
			
			
			
			c.onload = function()
			{
				counter += 1;
				Ti.API.info('IN ONLOAD ');
				Ti.API.info('nativePath = ' + f.nativePath);
				if (Titanium.Platform.name == 'android') {
					f.write(c.responseData);
					c.responseData = null;
				}
				
				if(counter<number)
				{
					//c = null;
					actInd.setValue(counter);
					actInd.setMessage('Lade ' + counter + ' von ' + number) ; 
					download(57);
				}else
				{
					c = null;
					Ti.App.Properties.setString("audioComplete","yes")
					ind.hide();	
					actInd.hide();
					//complete.show();
					return;
				}
	
			};
			c.ondatastream = function(e)
			{
				
				ind.value = e.progress;
				
				//Ti.API.info('location ' + c.location);
			};
			c.onerror = function(e)
			{
				Ti.API.info('XHR Error ' + e.error);
				ind.hide();	
				actInd.hide();
				return;
			};
			//c.onreadystatechange = function(){
			//	if(c.readyState == 4){
			//	}
			//};
			// open the client
			if (Titanium.Platform.name == 'android') {
				//android's WebView doesn't support embedded PDF content
				c.open('GET','http://195.98.194.214/audioguide/' + counter + '/' + counter + '_de.mp3');
			} else {
				c.open('GET','http://195.98.194.214/audioguide/' + counter + '/' + counter + '_de.mp3');
				c.file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,counter+'.mp3');
			}
	
			// send the data
			c.send();
		}
		
	}
	
	var counter = 1;
	//var i=stationsArray.length;
	/*
	while(counter<stationsArray.length){
		var audioFile = stationsArray[counter];
		if(download(audioFile))
		{
			counter++;
		}
	}
	*/
	var complete = false;
	//var audioFile = 'kloster_klausener_und_wieseneck';
	download(57);
});

