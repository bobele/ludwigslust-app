
var win = Titanium.UI.currentWindow;

var auswahl = win.auswahl;
var stationsMarkers=[];
var mLat = 0;
var mLong = 0;

if (stationsMarkers.length == 0) {

	var db = Titanium.Database.open('contentDB');
	var dbrows = db.execute('SELECT * FROM food'); 
	
	while (dbrows.isValidRow()) {
			var titel = '';
			titel = dbrows.fieldByName('name');
			var dbMarker = Titanium.Map.createAnnotation({
				animate:true,
				title:titel,
				myid:dbrows.fieldByName('id'),
				pincolor:Titanium.Map.ANNOTATION_RED,
				image:'img/pin.png',
				latitude:parseFloat(dbrows.fieldByName('lat')/1000000),
				longitude:parseFloat(dbrows.fieldByName('long')/1000000),
			  });
			stationsMarkers.push(dbMarker);
			
			//Titanium.API.info("Found : "+dbrows.fieldByName('station') + " Lat: "+dbrows.fieldByName('lat') + " Long: " + parseFloat(dbrows.fieldByName('long')) );
			dbrows.next();
	}
	dbrows.close();
	db.close();
}


var mapView = Ti.Map.createView({
	mapType: Ti.Map.STANDARD_TYPE,
	region:{
		latitude:53.325500, longitude:11.487600,
		latitudeDelta:0.01, longitudeDelta:0.01
	},
	animate:true,
	regionFit:true,
	userLocation:true
});
mapView.annotations = stationsMarkers;


win.add(mapView);
win.addEventListener('close', function(e)
    {
    win.remove(mapView);
    })
