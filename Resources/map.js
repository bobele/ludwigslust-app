var win = Titanium.UI.currentWindow;
var auswahl = win.auswahl;
var stationsMarkers = [];
var mLat = 0;
var mLong = 0;

if (stationsMarkers.length == 0) {

	var db = Titanium.Database.open('contentDB');
	if (auswahl != 0) {
		var dbrows = db.execute('SELECT stations_tours.*, stations.* FROM stations_tours, stations WHERE stations_tours.station_id = stations.id AND stations_tours.tour =' + auswahl);
	} else {
		var dbrows = db.execute('select * from stations');
	}
	while (dbrows.isValidRow()) {

		var titel = '';
		titel = dbrows.fieldByName('station');
		var dbMarker = Titanium.Map.createAnnotation({
			animate : true,
			title : titel,
			myid : dbrows.fieldByName('id'),
			pincolor : Titanium.Map.ANNOTATION_RED,
			image : 'img/pin_black.png',
			rightButton : 'img/speaker.png',
			latitude : parseFloat(dbrows.fieldByName('lat')),
			longitude : parseFloat(dbrows.fieldByName('long')),
		});
		stationsMarkers.push(dbMarker);

		//Titanium.API.info("Found : "+dbrows.fieldByName('station') + " Lat: "+dbrows.fieldByName('lat') + " Long: " + parseFloat(dbrows.fieldByName('long')) );
		dbrows.next();
	}
	dbrows.close();
	db.close();
}

var mapView = Ti.Map.createView({
	mapType : Ti.Map.STANDARD_TYPE,
	region : {
		latitude : 53.325500,
		longitude : 11.487600,
		latitudeDelta : 0.01,
		longitudeDelta : 0.01
	},
	animate : true,
	regionFit : true,
	userLocation : true
});
mapView.annotations = stationsMarkers;

mapView.addEventListener('click', function(evt) {

	var annotation = evt.annotation;
	Titanium.API.debug('annotation clicked');
	Titanium.API.debug(evt.annotation.latitude + ' ' + evt.annotation.longitude);

	var win = Ti.UI.createWindow({
		url : 'station.js',
	});
	win.currentId = evt.annotation.myid;
	win.currentStation = evt.annotation.title;
	win.open({
		modal : true
	});
});

win.add(mapView);
win.addEventListener('close', function(e) {
	win.remove(mapView);
})