var win = Titanium.UI.currentWindow;
win.backgroundColor='#8eca66';


var labelUnderground = Ti.UI.createView({
	backgroundColor: '#000000',
	top: 0,
	width:Titanium.Platform.displayCaps.platformWidth,
	height:40,
	opacity:0.5,
});
var label = Titanium.UI.createLabel({
	font:{fontSize:18},
	top:5,
	bottom:5,
	text:win.currentStation,
	color:'#ffffff',
	opacity:1,
});


var img = Ti.UI.createImageView({
      image:'img/station/bild_' + win.currentId + '.jpg'
});

var playUnderground = Ti.UI.createView({
	backgroundColor: '#000000',
	bottom: 0,
	width:Titanium.Platform.displayCaps.platformWidth,
	height:'50',
	opacity:0.7,
});
var play = Ti.UI.createImageView({
      backgroundImage:'/images/play.png',
      center:true,
      width:46/2,
      height:92/2,
      opacity:1,
});

var url = Titanium.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory,win.currentId + '.mp3');
// load from remote url
var sound = Titanium.Media.createSound({url:url});

var state = 0;
play.addEventListener('click', function()
{

	switch (state)
	{
		case 0:
		{
			sound.play();
			play.backgroundImage='/images/stop.png';
			play.width = 40;
			play.height = 40;
			state++;
			break;
		}
		case 1:
		{
			sound.stop();
			
			play.backgroundImage='/images/play.png';
	      	play.width=23,
      		play.height=46,
			state--;
			break;
		}
	};
	//pb.max = sound.duration;
});


win.add(img);

labelUnderground.add(label);
win.add(labelUnderground);
if(url.exists() == true)
{
playUnderground.add(play);
win.add(playUnderground);	
}

