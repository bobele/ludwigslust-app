// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#66ff33');

//
// create base UI tab and root window
//
var db = Titanium.Database.install('ludwigslust.db','contentDB');
db.close();
var screenWidth = Titanium.Platform.displayCaps.platformWidth;

var row1 = (screenWidth/4)-60;
var row2 = (screenWidth/4)*3-60;

var scrollView = Titanium.UI.createScrollView({
	contentWidth:'auto',
	contentHeight:'auto',
	top:0,
	showVerticalScrollIndicator:true,
	showHorizontalScrollIndicator:true
});

var view = Ti.UI.createView({
	width:screenWidth,
	height:'auto',
});


scrollView.add(view);

var win1 = Titanium.UI.createWindow({  
    title:'Ludwigslust',
    backgroundColor:'#8eca66',
});

var IMAGE = "images/wappen.png";

var wappen = Titanium.UI.createImageView({
	image:IMAGE,
	top:30,
	left:row1,
	width:120,
	height: 120,
});

var IMAGE = "/images/genuss.png";

var genuss = Titanium.UI.createImageView({
	image:IMAGE,
	top:40,
	left:row2,
	width:110,
	height: 100,
});


view.add(wappen);
view.add(genuss);



var IMAGE = "/images/guide.png";

var guide = Titanium.UI.createImageView({
	image:IMAGE,
	top:160,
	left:row1,
	width:110,
	height: 100,
});

var IMAGE = "/images/service.png";

var service = Titanium.UI.createImageView({
	image:IMAGE,
	top:160,
	left:row2,
	width:110,
	height: 100,
});


view.add(guide);
view.add(service);





var IMAGE = "/images/impression.png";

var impression = Titanium.UI.createImageView({
	image:IMAGE,
	top:290,
	left:row1,
	width:110,
	height: 100,
});

var IMAGE = "/images/veranstaltung.png";


view.add(impression);



var IMAGE = "/images/download.png";

var download = Titanium.UI.createImageView({
	image:IMAGE,
	top:290,
	left:row2,
	width:110,
	height: 100,
});

view.add(download);


genuss.addEventListener('click', function()
{
	var window = Ti.UI.createWindow({
		url:'map3.js',
	});
	window.open({modal:true});
});


guide.addEventListener('click', function()
{
	var window = Ti.UI.createWindow({
		url:'guide.js',
	});
	window.open({modal:true});
});
service.addEventListener('click', function()
{
	var window = Ti.UI.createWindow({
		url:'service.js',
	});
	window.open({modal:true});
});
impression.addEventListener('click', function()
{
	var window = Ti.UI.createWindow({
		url:'imp.js',
	});
	window.open({modal:true});
});

download.addEventListener('click', function()
{
	var window = Ti.UI.createWindow({
		url:'download.js',
	});
	window.open({modal:true});
});

win1.add(scrollView);
// open tab group
win1.open();
