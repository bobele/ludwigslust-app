var win = Titanium.UI.currentWindow;
win.setBackgroundColor('#fff');
var stationsArray = [];
var sRows = [];

// populate category array from database
// only called on first rendering of the tab, after that the array is filled
if (stationsArray.length == 0) {
	var db = Titanium.Database.open('contentDB');
	var dbrows = db.execute('SELECT * FROM stations ORDER BY station ASC');
	while (dbrows.isValidRow()) {
	    stationsArray.push({
	    	catid:dbrows.fieldByName('_id'),
	    	title:dbrows.fieldByName('station')
	    }); 
	    //Ti.API.info("Found museen: "+dbrows.fieldByName('slug')+" ["+dbrows.fieldByName('slug')+"]");
	    dbrows.next();
	}
	dbrows.close();
	db.close();
}
var firstLetter;
var oldFirstLetter;
// category table view
for (var c=0;c<stationsArray.length;c++) {
	var row = Ti.UI.createTableViewRow({color:'#333',title:stationsArray[c].title,hasChild:true,catid:stationsArray[c].catid}); 
	sRows[c] = row;
}


var stationTableView = Titanium.UI.createTableView({data:sRows});
// create table view

// add table view to the window
Titanium.UI.currentWindow.add(stationTableView);

