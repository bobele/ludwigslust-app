var win = Titanium.UI.currentWindow;

var data = [
	{title:'Taxi', hasChild:true, url:'map2.js', auswahl:'0',color:'#fff'},
	{title:'Banken', hasChild:true, url:'map2.js', auswahl:'1', color:'#fff'},
	{title:'Apotheken', hasChild:true, url:'map2.js', auswahl:'2', color:'#fff'},
	{title:'Kirchen', hasChild:true, url:'map2.js', auswahl:'3', color:'#fff'}
];


            
            
           
// create table view
var tableview = Titanium.UI.createTableView({
	data:data,
	backgroundColor:'#8eca66',
	
});

// create table view event listener
tableview.addEventListener('click', function(e)
{
	if (e.rowData.url)
	{
		var win = null;
		if (Ti.Platform.name == "android") {
			win = Titanium.UI.createWindow({
				url:e.rowData.url,
				title:e.rowData.title
			});
		} else {
			win = Titanium.UI.createWindow({
				url:e.rowData.url,
				title:e.rowData.title,
				backgroundColor:'#fff',
				barColor:'#111'
			});
		}
		win.auswahl = e.rowData.auswahl;
		win.open({modal:true, animated:true});
	}
});
win.add(tableview);
