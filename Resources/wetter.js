var win = Titanium.UI.currentWindow;
win.setBackgroundColor('#8eca66');

var wetter = 'http://www.google.com/ig/api?weather=ludwigslust&hl=de';

var label = Titanium.UI.createLabel({
	top:20,
	text:'',
	color:'#fff',
});


var current = Titanium.UI.createLabel({
	top:20,
	text:'',
	color:'#fff',
});

var day0 = Titanium.UI.createLabel({
	top:40,
	text:'heute',
	color:'#fff',
	left: 110,
});

var day0image = Titanium.UI.createImageView({
	top:40,
	left:5,
	width:100,
	height:100
});
win.add(day0image);

var day1 = Titanium.UI.createLabel({
	top:150,
	text:'',
	color:'#fff',
});

var day1image = Titanium.UI.createImageView({
	top:150,
	width:100,
	height:100,
	left:5,
});
	
win.add(day1image);
var day2= Titanium.UI.createLabel({
	top:260,
	text:'',
	color:'#fff',
});
var day2image = Titanium.UI.createImageView({
	top:260,
	width:100,
	height:100,
	left:5,
});
	
win.add(day2image);
Titanium.Network.addEventListener('change', function(e)
{
	var type = e.networkType;
	var online = e.online;
	var networkTypeName = e.networkTypeName;
	if(online==true)
	{
	label.text = 'Die aktuellen Wetterdaten werden geladen.';
	}
	else{
	label.text = 'Es besteht keine Internetverbindung';
	}
});


win.add(label);
win.add(current);
win.add(day0);
win.add(day1);
win.add(day2);

function utf8_encode ( string ) {
    // Encodes an ISO-8859-1 string to UTF-8  
    // 
    // version: 812.316
    // discuss at: http://phpjs.org/functions/utf8_encode
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: sowberry
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman
    // +   improved by: Yves Sucaet
    // +   bugfixed by: Onno Marsman
    // *     example 1: utf8_encode('Tom van Mikes');
    // *     returns 1: 'Tom van Mikes'
    string = (string+'').replace(/\r\n/g, "\n").replace(/\r/g, "\n");

    var utftext = "";
    var start, end;
    var stringl = 0;

    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
            end++;
        } else if((c1 > 127) && (c1 < 2048)) {
            enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
        } else {
            enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
        }
        if (enc != null) {
            if (end > start) {
                utftext += string.substring(start, end);
            }
            utftext += enc;
            start = end = n+1;
        }
    }

    if (end > start) {
        utftext += string.substring(start, string.length);
    }

    return utftext;
}

var xhr = Ti.Network.createHTTPClient();
xhr.open("GET",wetter);
xhr.setRequestHeader("Content-Type", "text/xml");
xhr.setRequestHeader("charset", "utf-8");
xhr.onload = function()
{
	try
	{
		var doc = this.responseXML.documentElement;
		var current_conditions = doc.getElementsByTagName("current_conditions");
		var weather_now_cond = current_conditions.item(0).getElementsByTagName("condition").item(0).getAttribute('data');
		var weather_now_tempc = current_conditions.item(0).getElementsByTagName("temp_c").item(0).getAttribute('data');
		var weather_now_humid = current_conditions.item(0).getElementsByTagName("humidity").item(0).getAttribute('data');
		
		var forecast_conditions = doc.getElementsByTagName("forecast_conditions");
		for (var c=0,i=forecast_conditions.length;c<i;c++)
	    {
	      var item = forecast_conditions.item(c);
	      var item_day = item.getElementsByTagName("day_of_week").item(0).getAttribute('data');
	      var item_low = item.getElementsByTagName("low").item(0).getAttribute('data');
	      var item_high = item.getElementsByTagName("high").item(0).getAttribute('data');
	      var item_cond = item.getElementsByTagName("condition").item(0).getAttribute('data');
	      var item_icon = "http://www.google.com" + item.getElementsByTagName("icon").item(0).getAttribute('data');
	      switch(c){
	      	case 0:
	      		day0.text = "Tag: " +item_day + "\n" + item_cond + "\nTemp. max."  + item_high + "\nTemp min." + item_low;
	      		day0image.image=item_icon;
	      		break;
	      	case 1:
	      		day1.text = "Tag: " +item_day + "\n" + item_cond + "\nTemp. max."  + item_high + "\nTemp min." + item_low;
	      		day1image.image=item_icon;
	      		break;
	      	case 2:
	      		day2.text = "Tag: " +item_day + "\n" + item_cond + "\nTemp. max."  + item_high + "\nTemp min." + item_low;
	      		day2image.image=item_icon;
	      		break;
	      }
	    }
	    win.remove(label);
		current.text = utf8_encode("Wetter: "+ weather_now_cond + " | Temp:" + weather_now_tempc);
	
	}
	catch(E)
	{
		alert(E);
	}
};
xhr.send();
