
var defaultColor = "#fff";
var data = [];
var win = Titanium.UI.currentWindow;

var breite = Titanium.Platform.displayCaps.platformWidth;
Ti.API.info('Breite :' + breite);
breite = Math.floor(breite/60);
var space =  Titanium.Platform.displayCaps.platformWidth - (breite*60);
if(space!==0)
{
  space = Math.floor(space/breite);
}


Ti.API.info('Anzahl Elemente pro Zeile :' + breite);
Ti.API.info('Anzahl der Zeilen :' + Math.floor(10/breite));
var countPics = win.count;
var path = win.path;
var count = 1;
while(count < countPics){
for(var i=1,c = Math.ceil(countPics/breite);i<=c;i++)
{
  var row = Ti.UI.createTableViewRow({
        backgroundColor:'#8eca66'
  });
  for(var k=0;k<breite;k++)
  {
  if (Titanium.Platform.name == 'android')
          {
            // iphone moved to a single image property - android needs to do the same
          var img = Ti.UI.createImageView({
              id:count,
              url:'img/impression_thumb/'+ path+'/'+count+'.jpg',
              left:(k*60)+(k*space),
            });
          }
          else
          {
            var img = Ti.UI.createImageView({
              id:count,
              image:'img/impression_thumb/'+ path+'/'+count+'.jpg',
              left:(k*60)+(k*space),
            });
          }
          row.add(img);
          count++;
  }
  data.push(row);
}
}
//
// create table view (
//
tableView = Titanium.UI.createTableView({
  data:data,
  backgroundColor:'#8eca66'
});

win.add(tableView);

tableView.addEventListener('click', function(e)
{
  Ti.API.info('table view row clicked - source ' + e.source +" ID: "+ e.source.id);
  var win2 = Titanium.UI.createWindow();
  if (Titanium.Platform.name == 'android')
          {
            // iphone moved to a single image property - android needs to do the same
          var img = Ti.UI.createImageView({
              id:count,
              url:'img/impression/'+ path+'/'+e.source.id+'.jpg'
            });
          }
          else
          {
            var img = Ti.UI.createImageView({
              id:count,
              image:'img/impression/'+ path+'/'+e.source.id+'.jpg'
            });
          }
  win2.add(img);
  win2.open({modal:true,animated:true});
  // use rowNum property on object to get row number

});
